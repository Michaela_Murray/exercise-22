﻿using System;

namespace exercise_22
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var movies = new string[5] { "Saw", "Harry Potter", "Avengers", "Captain America", "Zoolander"};
            
            Array.Sort(a);
            
            Console.WriteLine(string.Join(", ", a));


            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
